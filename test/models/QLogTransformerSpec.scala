package models

import play.api.test.PlaySpecification
import models.QueueLog._

class QLogTransformerSpec extends PlaySpecification {

  "qlog transformer" should {
    "return unkown event on unknown" in {
      val qLogData = QueueLogData("2014-04-18 12:22:20.225112", "NONE", "Local/id-44", Some("UNKNOWN"), None, None, Some(1))

      QLogTransformer.transform(qLogData) should be_==(Unknown)
    }

    "return enterQueue on event ENTERQUEUE" in {
      val qLogData = QueueLogData("2014-04-18 12:22:20.225112", "NONE", "ENTERQUEUE", None, None, None, Some(3))
      QLogTransformer.transform(qLogData) should be_==(EnterQueue(3))
    }

    "return abandonned on event ABANDON" in {
      val qLogData = QueueLogData("2014-05-20 01:34:20.235112", "NONE", "ABANDON", Some("1"), Some("3"), Some("34"), Some(5))
      QLogTransformer.transform(qLogData) should be_==(Abandonned(5, 34))
    }
    "return complete on event COMPLETEAGENT" in {
      val qLogData = QueueLogData("2014-05-22 11:34:20.235112", "Agent/2500", "COMPLETEAGENT", Some("5"), Some("12"), Some("1"), Some(7))
      QLogTransformer.transform(qLogData) should be_==(Complete(7, "2500", 5, 12))
    }
    "return complete on event COMPLETECALLER" in {
      val qLogData = QueueLogData("2014-05-22 13:32:19.045234", "Agent/2850", "COMPLETECALLER", Some("4"), Some("32"), Some("1"), Some(8))
      QLogTransformer.transform(qLogData) should be_==(Complete(8, "2850", 4, 32))
    }
    "return connect on event CONNECT" in {
      val qLogData = QueueLogData("2014-05-22 14:44:54.774283", "Agent/3250", "CONNECT", Some("12"), Some("1400762693.17"), Some("1"), Some(9))
      QLogTransformer.transform(qLogData) should be_==(Connect(9, "3250", 12, "1400762693.17"))
    }
    "return closed on event CLOSED" in {
      val qLogData = QueueLogData("2014-05-22 14:44:54.774283", "NONE", "CLOSED", None, None, None, Some(11))
      QLogTransformer.transform(qLogData) should be_==(Closed(11))
    }
    "return timeout on event EXITWITHTIMEOUT" in {
      val qLogData = QueueLogData("2014-05-22 14:44:54.774283", "NONE", "EXITWITHTIMEOUT", Some("1"), Some("1"), Some("40"), Some(22))
      QLogTransformer.transform(qLogData) should be_==(Timeout(22,40))
    }
    "return timeout with 0 waiting time when no waiting time in database" in {
      val qLogData = QueueLogData("2014-05-22 14:44:54.774283", "NONE", "EXITWITHTIMEOUT", Some("1"), Some(""), Some(""), Some(22))
      QLogTransformer.transform(qLogData) should be_==(Timeout(22,0))
    }

  }
}
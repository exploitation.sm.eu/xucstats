package stats

import akka.actor._
import akka.testkit.{TestActorRef, TestProbe}
import akkatest.TestKitSpec
import com.codahale.metrics._
import models.QueueLog._
import org.mockito.Matchers
import org.mockito.Matchers.any
import org.mockito.Mockito._
import org.scalatest.mock.MockitoSugar
import services.XucStatsEventBus.{StatUpdate, Stat, AggregatedStatEvent}
import xivo.models.XivoObject.ObjectDefinition
import stats.Statistic._
import xivo.models.XivoObject.ObjectType._
import com.codahale.metrics.MetricRegistry

class StatisticSpec extends TestKitSpec("StatisticSpec")
  with MockitoSugar {


  class Helper(objDef: ObjectDefinition, statRef: String) {


    trait MockHisto extends HistogramCalculation {
      def getval():StatValue =  0.0

      def UpdateWindow(value: StatValue) = mock[(StatValue)=>Unit]
      def getHistoValue(): StatValue = 10.0
      def valueChanged = true
      def updateHisto = {}

    }
    val aggregator = TestProbe()
    val calculator = mock[Statistic.StatCalculator]
    val registry = mock[MetricRegistry]
    val window = mock[Histogram]
    val gauge = new DoubleGauge()
    val snapshot = mock[Snapshot]

    val statFn = Statistic.TotalNumberCallsEntered

    def actor() = {

      when(registry.register(Matchers.eq(s"${Statistic.statTreeRef(objDef,statRef)}.slidingHour"),any[Histogram])).thenReturn(window)
      when(registry.register(Matchers.eq(s"${Statistic.statTreeRef(objDef,statRef)}.count"),any[DoubleGauge])).thenReturn(gauge)
      when(registry.register(Matchers.eq(s"${Statistic.statTreeRef(objDef,statRef)}.lastHour"),any[DoubleGauge])).thenReturn(new DoubleGauge())

      val a = TestActorRef(new Statistic(statRef, objDef, calculator, aggregator.ref, registry) with MockHisto)
      (a, a.underlyingActor)
    }

    def pushStat(value: StatValue, stat: ActorRef) = {
      val acc = (0.toLong, 0.toLong)
      when(calculator(any[ObjectEvent], any[Accumulator])).thenReturn(Some(acc, value))

      stat ! EnterQueue(objDef.objectRef.getOrElse(0))
      aggregator.expectMsgAllOf(StatUpdate(objDef, List(Stat(statRef, value))),StatUpdate(objDef, List(Stat(s"${statRef}LastHour", 10.0))))

    }
  }

  override def afterAll() { system.shutdown() }

  "Statistic" should {
    val queueName = "blue"
    val objDef = ObjectDefinition(Queue, Some(42))

    "Increment value on EnterQueue event " in new Helper(objDef, "TotalNumberCallsEntered") {
      val (ref, _) = actor()
      when(calculator(any[ObjectEvent], any[Accumulator])).thenReturn(Some((1L, 0L), 1.0))

      ref ! EnterQueue(42)

      aggregator.expectMsgAllOf(StatUpdate(objDef, List(Stat("TotalNumberCallsEntered", 1))),StatUpdate(objDef, List(Stat("TotalNumberCallsEnteredLastHour", 10.0))))
      aggregator.expectNoMsg
    }

    "use the accumulator on stat calculation and keep it on result" in new Helper(objDef, "TotalNumberCallsEntered") {
      val (ref, statistic) = actor()
      statistic.acc = (32L, 56L)
      val event = Connect(42, "2850", 5, "5464665465.22")
      when(calculator(any[ObjectEvent], any[Accumulator])).thenReturn(Some((33L, 56L), 33.0))

      ref ! event

      verify(calculator)(event, (32L, 56L))
      statistic.acc should be((33L, 56L))
    }

    "reset stat on demand" in new Helper(objDef, "TotalCalls") {
      val (ref, statistic) = actor()
      pushStat(88.0, ref)
      statistic.acc = (32L, 56L)

      ref ! ResetStat

      aggregator.expectMsg(StatUpdate(objDef, List(Stat("TotalCalls", 0.0))))
      statistic.acc should be((0L, 0L))
    }
  }

  "statistic TotalNumberCallsEntered functions" should {
    "increment the accumulator and value" in {
      val statFn = Statistic.TotalNumberCallsEntered

      statFn(EnterQueue(43), (32, 0)) should be(Some((33L, 0L), 33.0))
    }

    "do not increment the accumulator for another event" in {

      val statFn = Statistic.TotalNumberCallsEntered

      statFn(Abandonned(43, 10), (10, 0)) should be(None)
    }
  }

  "statistic TotalNumberCallsAnswered functions" should {
    "increment the accumulator and value" in {
      val statFn = Statistic.TotalNumberCallsAnswered

      statFn(Connect(44, "2850", 5, "11111111111.55"), (22, 0)) should be(Some((23L, 0L), 23.0))
    }
  }
  "statistic TotalNumberCallsClosed functions" should {
    "increment the accumulator and value" in {
      val statFn = Statistic.TotalNumberCallsClosed

      statFn(Closed(44), (2, 0)) should be(Some((3L, 0L), 3.0))
    }
  }

  "statistic TotalNumberCallsTimeout functions" should {
    "increment the accumulator and value" in {
      val statFn = Statistic.TotalNumberCallsTimeout

      statFn(Timeout(55,40), (5, 0)) should be(Some((6L, 0L), 6.0))
    }
  }
  "statistic total number of answered before 30 s" should {
    "change the accumulator and increase value when call answered before 30s" in {
      val statFn = Statistic.TotalNumberCallsAnsweredBefore(30)
      statFn(Connect(44, "2850", 5, "11111111111.55"), (22, 0)) should be(Some((23L, 0L), 23.0))
    }
  }
  "statistic total number of answered before 30 s" should {
    "not change the accumulator and increase value when call answered after 30s" in {
      val statFn = Statistic.TotalNumberCallsAnsweredBefore(30)
      statFn(Connect(44, "2850", 35, "11111111111.55"), (22, 0)) should be(None)
    }
  }
  "statistic total number of abandonned after 12 s" should {
    "change the accumulator and increase value when call abandonned after 12 s" in {
      val statFn = Statistic.TotalNumberCallsAbandonnedAfter(12)
      statFn(Abandonned(45, 15), (22, 0)) should be(Some((23L, 0L), 23.0))
    }
  }
  "statistic total number of abandonned after 12 s" should {
    "not change the accumulator and value when call abandonned before 12 s" in {
      val statFn = Statistic.TotalNumberCallsAbandonnedAfter(12)
      statFn(Abandonned(45, 5), (22, 0)) should be(None)
    }
  }
  
  "statistic RelativeNumberPercentage of call answered before 15 s" should {
    "change the accumulator and increase value when call entered" in {
      val statFn = Statistic.RelativeNumberPercentageAnsweredBefore(15L)
      val acc = (3L, 4L) // 75 %

      statFn(EnterQueue(46), acc) should be(Some((3L, 5L), 60.0))
    }
    "do not change the value when answered after 15 sec" in {
      val statFn = Statistic.RelativeNumberPercentageAnsweredBefore(15L)
      val acc = (1L, 2L) // 50 %

      statFn(Connect(46, "2850", 25, "5464665465.22"), acc) should be(None)
    }
    "increment the accumulator and value increase when answered before 15 sec" in {
      val statFn = Statistic.RelativeNumberPercentageAnsweredBefore(15L)
      val acc = (3L, 4L) // 75 %

      statFn(Connect(46, "2850", 5, "5464665465.22"), acc) should be(Some((4L, 4L), 100.0))

    }
  }
  "Percentage of call abandonned after 20 seconds " should {
    "change the accumulator do not change value when abandonned before 20 sec" in {
      val statFn = Statistic.RelativeNumberPercentageAbandonnedAfter(15L)
      val acc = (3L, 4L) // 75 %

      statFn(Abandonned(47, 10), acc) should be(None)
    }
    "change the accumulator and increase value when abandonned after 20 sec" in {
      val statFn = Statistic.RelativeNumberPercentageAbandonnedAfter(15L)
      val acc = (2L, 4L) // 75 %

      statFn(Abandonned(48, 30), acc) should be(Some((3L, 4L), 75.0))
    }
    "change the accumulator and increase value when call entered" in {
      val statFn = Statistic.RelativeNumberPercentageAbandonnedAfter(15L)
      val acc = (3L, 4L) // 75 %

      statFn(EnterQueue(49), acc) should be(Some((3L, 5L), 60.0))
    }
  }
  "percent" should {
    "return 0 on divide by 0" in {
      Statistic.Percent(3,0) should be(0)
    }
  }
  
   "TotalWaitingTimeAnswered" should {
    "accumulate waitTime when a call is answered" in {
      val statFn = Statistic.TotalWaitingTimeAnswered
      val acc = (33L, 0L)

      statFn(Connect(0, "agent1", 44, "callId"), acc ) should be(Some((77L, 0L), 77.0))
    }
  }
   "TotalWaitingTimeAbandonned" should {
	   "accumulate waitTime when a call is abandonned" in {
		   val statFn = Statistic.TotalWaitingTimeAbandonned
				   val acc = (22L, 0L)
				   
				   statFn(Abandonned(0,  44), acc ) should be(Some((66L, 0L), 66.0))
	   }
   }
}
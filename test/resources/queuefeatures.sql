DROP TABLE if EXISTS queuefeatures;
CREATE TABLE queuefeatures
(
      id serial NOT NULL,
      name character varying(128) NOT NULL,
      displayname character varying(128) NOT NULL,
      number character varying(40) NOT NULL DEFAULT ''::character varying,
      context character varying(39),
      data_quality integer NOT NULL DEFAULT 0,
      hitting_callee integer NOT NULL DEFAULT 0,
      hitting_caller integer NOT NULL DEFAULT 0,
      retries integer NOT NULL DEFAULT 0,
      ring integer NOT NULL DEFAULT 0,
      transfer_user integer NOT NULL DEFAULT 0,
      transfer_call integer NOT NULL DEFAULT 0,
      write_caller integer NOT NULL DEFAULT 0,
      write_calling integer NOT NULL DEFAULT 0,
      url character varying(255) NOT NULL DEFAULT ''::character varying,
      announceoverride character varying(128) NOT NULL DEFAULT ''::character varying,
      timeout integer NOT NULL DEFAULT 0,
      preprocess_subroutine character varying(39),
      announce_holdtime integer NOT NULL DEFAULT 0,
      waittime integer,
      waitratio double precision,
      CONSTRAINT queuefeatures_pkey PRIMARY KEY (id)
);

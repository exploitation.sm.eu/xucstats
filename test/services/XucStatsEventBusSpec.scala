package services

import akka.testkit.TestProbe
import akkatest.TestKitSpec
import org.scalatest.BeforeAndAfter
import org.scalatest.mock.MockitoSugar
import services.XucStatsEventBus.{Stat, AggregatedStatEvent}
import stats.Statistic.RequestStat
import xivo.models.XivoObject.ObjectDefinition

class XucStatsEventBusSpec extends TestKitSpec("XucStatsEventBusSpec")
  with BeforeAndAfter with MockitoSugar {

  import xivo.models.XivoObject.ObjectType._

  var statsBus: XucStatsEventBus = _
  var actor: TestProbe = null

  before {
    statsBus = new XucStatsEventBus
    actor = TestProbe()
  }

  "XucStatsEventBus" should {

    "send stat to the subscriber on object type" in {

      val mockEvent = AggregatedStatEvent(ObjectDefinition(Queue, Some(1)), List[Stat]())
      statsBus.subscribe(actor.ref, ObjectDefinition(Queue))

      statsBus.publish(mockEvent)

      actor.expectMsg(mockEvent)
    }

    "send stat to the subscriber on object ref" in {
      val mockEvent = AggregatedStatEvent(ObjectDefinition(Queue, Some(2)), List[Stat]())
      statsBus.subscribe(actor.ref, ObjectDefinition(Queue, Some(2)))

      statsBus.publish(mockEvent)

      actor.expectMsg(mockEvent)

    }
    "not send stat to the subscriber on other object ref" in {
      val mockEvent = AggregatedStatEvent(ObjectDefinition(Queue, Some(3)), List[Stat]())
      statsBus.subscribe(actor.ref, ObjectDefinition(Queue, Some(99)))

      statsBus.publish(mockEvent)

      actor.expectNoMsg

    }

    "send stat to object type only subscriber" in {
      val mockEvent = AggregatedStatEvent(ObjectDefinition(Queue,Some(4)), List[Stat]())

      statsBus.subscribe(actor.ref, ObjectDefinition(Queue))

      statsBus.publish(mockEvent)
      actor.expectMsg(mockEvent)
    }
 
    "send subscription to bus manager" in {
      import services.XucStatsEventBus._
      val busManager = TestProbe()
      val subscriber = TestProbe()
      val objdef = ObjectDefinition(Queue, Some(5))

      statsBus.setBusManager(busManager.ref)

      statsBus.subscribe(subscriber.ref, objdef)

      busManager.expectMsg(RequestStat(subscriber.ref,objdef))
    }
  }
}
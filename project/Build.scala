import sbt._
import Keys._
import play.Play.autoImport._

object ApplicationBuild extends Build {

  val appName = "xucstats"
  val appVersion = "2.3.3"
  val appOrganisation = "xivo"

  val main = Project(appName, file("."))
    .enablePlugins(play.PlayScala)
    .settings(
      version := appVersion,
      scalaVersion := Dependencies.scalaVersion,
      organization := appOrganisation,
      resolvers     ++= Dependencies.resolutionRepos,
      libraryDependencies ++= Dependencies.runDep ++ Dependencies.testDep,
      sbt.Keys.fork in Test := false
    )
}

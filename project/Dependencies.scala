import sbt._
import play.Play.autoImport._

object Version {
  val scalaTest = "2.2.4"
  val mockito = "1.9.5"
  val postgresql = "9.1-901.jdbc4"
  val scalike = "2.0.7"
  val dbunit = "2.4.7"
  val akkaquartz = "0.3.0"
  val akka = "2.3.4"
  val metricsPlay = "2.4.0_0.3.0"
  val scalatestplay = "1.2.0"
}

object Library {
  val akkaTestkit       = "com.typesafe.akka"              %% "akka-testkit"           % Version.akka
  val scalaTest         = "org.scalatest"                  %% "scalatest"              % Version.scalaTest
  val mockito           = "org.mockito"                    %  "mockito-all"            % Version.mockito
  val akkaquartz        = "us.theatr"                      %% "akka-quartz"            % Version.akkaquartz
  val scalike           = "org.scalikejdbc"                %% "scalikejdbc"            % Version.scalike
  val postgresql        = "postgresql"                     %  "postgresql"             % Version.postgresql
  val dbunit            = "org.dbunit"                     %  "dbunit"                 % Version.dbunit
  val metricsPlay       = "com.kenshoo"                    %% "metrics-play"           % Version.metricsPlay
  val scalatestplay     = "org.scalatestplus"              %% "play"                   % Version.scalatestplay

}

object Dependencies {

  import Library._

  val scalaVersion = "2.11.6"

  val resolutionRepos = Seq(
    ("Local Maven Repository" at "file:///" + Path.userHome.absolutePath + "/.m2/repository"),
    ("theatr.us" at "http://repo.theatr.us")
  )

  val runDep = run(
    scalike,
    postgresql,
    akkaquartz,
    metricsPlay,
    jdbc,
    anorm
  )

  val testDep = test(
    scalaTest,
    akkaTestkit,
    mockito,
    dbunit,
    scalatestplay
  )

  def run       (deps: ModuleID*): Seq[ModuleID] = deps
  def test      (deps: ModuleID*): Seq[ModuleID] = deps map (_ % "test")

}

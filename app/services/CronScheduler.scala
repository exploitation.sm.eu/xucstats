package services

import akka.actor.ActorRef
import play.api.Play.current
import play.api.libs.concurrent.Akka
import akka.actor.Props
import us.theatr.akka.quartz.QuartzActor

trait CronScheduler {
  val scheduler:ActorRef
}

trait ProductionCronScheduler extends CronScheduler {
  val scheduler = Akka.system.actorOf(Props[QuartzActor])
}


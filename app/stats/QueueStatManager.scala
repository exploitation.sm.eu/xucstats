package stats

import Statistic.ResetStat
import akka.actor.Actor
import akka.actor.ActorLogging
import akka.actor.ActorRef
import akka.actor.Props
import akka.actor.actorRef2Scala
import services.CronScheduler
import services.ProductionCronScheduler
import stats.Statistic.PublishSlidingStats
import us.theatr.akka.quartz.AddCronSchedule
import xivo.models.XivoObject.ObjectDefinition
import xivo.models.XivoObject.ObjectType.Queue
import xivo.xucstats.XucStatsConfig

object QueueStatManager {
  def apply(queueId: Int) = new QueueStatManager(queueId) with ProductionCronScheduler
}

class QueueStatManager(queueId: Int) extends Actor with ActorLogging {
  this: CronScheduler =>
  import xivo.models.XivoObject.ObjectType._
  import Statistic.ResetStat

  val WaitTimeThreshold = 15
  val customWaitTimeThreshold = XucStatsConfig.customWaitTimeThreshold
  val PublishSlidingSchedule = "0 * * * * ?"


  var stats: List[ActorRef] = List()

  override def preStart() {
    log.info(s"setting schedule to ${XucStatsConfig.resetSchedule}")
    scheduler ! AddCronSchedule(self, XucStatsConfig.resetSchedule, ResetStat)
    scheduler ! AddCronSchedule(self, PublishSlidingSchedule, PublishSlidingStats)
  }

  val aggregator = context.actorOf(StatsAggregator.props(), s"StatsAggregator-$queueId")

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsEntered", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsEntered, aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsAbandonned", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsAbandonned, aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic(s"TotalNumberCallsAbandonnedAfter$WaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsAbandonnedAfter(WaitTimeThreshold), aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsAnswered", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsAnswered, aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic(s"TotalNumberCallsAnsweredBefore$WaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsAnsweredBefore(WaitTimeThreshold), aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic(s"PercentageAnsweredBefore$WaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.RelativeNumberPercentageAnsweredBefore(WaitTimeThreshold), aggregator) with HistoValue)) :: stats

  stats = context.actorOf(Props(new Statistic(s"PercentageAbandonnedAfter$WaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.RelativeNumberPercentageAbandonnedAfter(WaitTimeThreshold), aggregator) with HistoValue)) :: stats

  stats = context.actorOf(Props(new Statistic(s"TotalNumberCallsAnsweredBefore$customWaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsAnsweredBefore(customWaitTimeThreshold), aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic(s"PercentageAnsweredBefore$customWaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.RelativeNumberPercentageAnsweredBefore(customWaitTimeThreshold), aggregator) with HistoValue)) :: stats

  stats = context.actorOf(Props(new Statistic(s"PercentageAbandonnedAfter$customWaitTimeThreshold", ObjectDefinition(Queue, Some(queueId)),
    Statistic.RelativeNumberPercentageAbandonnedAfter(customWaitTimeThreshold), aggregator) with HistoValue)) :: stats

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsClosed", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsClosed, aggregator) with HistoSize)) :: stats

  stats = context.actorOf(Props(new Statistic("TotalNumberCallsTimeout", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalNumberCallsTimeout, aggregator) with HistoSize)) :: stats
 
  stats = context.actorOf(Props(new Statistic("TotalWaitingTimeAnswered", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalWaitingTimeAnswered, aggregator) with HistoValue)) :: stats
        
  stats = context.actorOf(Props(new Statistic("TotalWaitingTimeAbandoned", ObjectDefinition(Queue, Some(queueId)),
    Statistic.TotalWaitingTimeAbandonned, aggregator) with HistoValue)) :: stats

  def receive = {
    case event => stats.foreach(_ ! event)
  }
}
package xivo.xucstats

import play.api.Play.current

object XucStatsConfig {
  def resetSchedule: String = current.configuration.getString("xucstats.resetSchedule").getOrElse("0 0 0 * * ?")
  val initFromMidnight:Boolean = current.configuration.getBoolean("xucstats.initFromMidnight").getOrElse(false)

  val aggregationPeriod: Long = current.configuration.getLong("xucstats.aggregationPeriod").getOrElse(1)

  val statsLogReporter = current.configuration.getBoolean("metrics.logReporter").getOrElse(true)
  val statsLogReporterPeriod: Long = current.configuration.getLong("metrics.logReporterPeriod").getOrElse(5)
  
  val customWaitTimeThreshold:Int =  current.configuration.getInt("xucstats.customWaitTimeThreshold").getOrElse(60)
}